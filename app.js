const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const port = process.env.PORT || 3000
require("./config/config");

//Connection
mongoose.connect(
    process.env.URLBD,
    {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true},
    (err, res) => {
      if (err) throw err;
  
      console.log('Base de datos conectada');
    }
  );

const app = express();


// Import Models
let Article = require('./models/article');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Middleware y JSON
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Public Folder
app.use(express.static(path.join(__dirname, 'public')));

//Routes
app.get('/', function(req, res) {
    Article.find({}, function(err, articles) {
        if (err) {
            console.log(err);
        } else {
            res.render('index', {
                title: 'Articles',
                articles: articles
            });
        }
    });

});

// Route Add
app.get('/articles/add', (req, res) => {
    res.render('add_article', {
        title: 'Add Article'

    });
});

// Methods DB
//GET
app.get('/article/:id', (req, res) => {
    Article.findById(req.params.id, function(err, article) {
        res.render('article', {
            article: article
        });
    });
});

//Add article
app.post('/articles/add', (req, res) => {
    let article = new Article();
    article.title = req.body.title;
    article.author = req.body.author;
    article.body = req.body.body;

    article.save(function(err) {
        if (err) {
            console.log(err);
            return;
        } else {
            res.redirect('/');
        }
    })
});


//Edit
app.get('/article/edit/:id', (req, res) => {
    Article.findById(req.params.id, function(err, article) {
        res.render('edit_article', {
            title: 'Editar Articulo',
            article: article
        });
    });
});

//Update
app.post('/articles/edit/:id', (req, res) => {
    let article = {};
    article.title = req.body.title;
    article.author = req.body.author;
    article.body = req.body.body;

    let query = { _id: req.params.id }

    Article.update(query, article, function(err) {
        if (err) {
            console.log(err);
            return;
        } else {
            res.redirect('/');
        }
    })
});

//Detele
app.delete('/article/:id', function(req, res) {
    let query = { _id: req.params.id }
    Article.remove(query, function(err) {
        if (err) {
            console.log(err);
        }
        res.send('Success');
    });
});

app.listen(port, () => console.log(`Escuchando puerto ${port}`))